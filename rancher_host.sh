#!/bin/bash

apt-get update && apt-get -y upgrade
apt -y install docker.io
systemctl start docker
systemctl enable docker
docker run -d --restart=unless-stopped -p 8080:8080 rancher/server