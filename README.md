Command to launch script

curl -s -L https://gitlab.com/path-to-script.sh | bash

Simple host (worker):
curl -s -L https://gitlab.com/YetAnotherGitUser/ubuntu-install-script/-/raw/master/host.sh | bash

Simple host (rancher):
curl -s -L https://gitlab.com/YetAnotherGitUser/ubuntu-install-script/-/raw/master/rancher_host.sh | bash