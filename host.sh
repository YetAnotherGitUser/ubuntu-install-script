#!/bin/bash

apt-get update && apt-get -y upgrade
apt -y install docker.io
systemctl start docker
systemctl enable docker
